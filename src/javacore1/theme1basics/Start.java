package javacore1.theme1basics;

import java.util.Scanner;

/**
 * Стартовый класс калькулятора
 * @author Chelnokov Egor
 */

public class Start {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the first number: ");
        double first = reader.nextDouble();
        System.out.println("Enter the second number: ");
        double second = reader.nextDouble();

        printResult(first, second);
    }

    private static void printResult(double first, double second) {
        System.out.println("summ = " + Calc.add(first, second));
        System.out.println("difference = " + Calc.subtract(first, second));
        System.out.println("multiplication = " + Calc.multiply(first, second));
        System.out.println("quotient = " + Calc.divide(first, second));
        System.out.println("remainder = " + Calc.giveRemainder(first, second));
        System.out.println(first + " in degree " + second + " = " + Calc.pow(first, second));
    }
}
