package javacore1.theme1basics;

/**
 * Java Core1
 * Основы 1. Структура программы, синтаксис, типы данных
 *
 * Лабораторная работа
 */
public class Calc {

    static double add(double a, double b) {
        return a + b;
    }

    static double subtract(double a, double b) {
        return a - b;
    }

    static double multiply(double a, double b) {
        return a * b;
    }

    static double divide(double a, double b) {
        return a / b;
    }

    static double pow(double a, double b) {
        return Math.pow(a,b);
    }

    static double giveRemainder(double a, double b) {
        return a % b;
    }
}
