package javacore1.theme7exceptions;

import java.util.Scanner;

/**
 * Java Core1
 * Исключения, утверждения, протоколирование и отладка
 *
 * Лабораторная работа
 */
public class Exceptions {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int size = scanner.nextInt();
        int[] numbers = new int[size];
        numbers[7] = Integer.parseInt(scanner.nextLine());
        int result = div(10, numbers[7]);
        System.out.println(result);
    }

    private static int div(int num1, int num2) {
        return num1 / num2;
    }
}
